+++
title = "About"
description = "About Alexander Roth"
date = "2023-01-16"
aliases = ["about", "about-me"]
author = "Alexander Roth"
slug = "about"
+++

## Welcome to my personal website! 

I am a research associate and PhD candidate at [DIW Berlin](https://www.diw.de/en). My research deals with the challenges of decarbonizing our electricity and energy system. 

Have a look at [my current and past research and publications](../research) or other [projects](../projects) I am involved in.

## Education

since 2018 &nbsp; **Ph.D. in Economics** &nbsp; ([DIW Berlin Graduate Center](https://www.diw.de/de/diw_01.c.619412.de/graduate_center.html) and [Berlin School of Economics Berlin](https://berlinschoolofeconomics.de/home))

2014-2016 &nbsp; **M.Sc. in Economics** ([University of Mannheim](https://www.uni-mannheim.de/en/))
 
2015-2016 &nbsp; **M.Sc. in Quantitative Economics** ([Université libre de Bruxelles (ULB)](https://www.ulb.be/) & [ECARES](https://ecares.ulb.be/))

2011-2014 &nbsp; **B.Sc. in Economics** ([University of Mannheim](https://www.uni-mannheim.de/en/))

## Professional

since 2019 &nbsp; [**Research Associate**](https://www.diw.de/sixcms/detail.php?id=diw_01.c.602199.en) - German Institute for Economic Research (DIW Berlin)  
Department *Energy, Transportation, Environment Department*

2016-2018 &nbsp; [**Research Assistant & Intern**](https://www.bruegel.org/author/alexander-roth) - Bruegel

2015-2016 &nbsp; [**Blue Book Trainee**](https://ec.europa.eu/clima/index_en) European Commission (*DG Climate Action*)
---
title: "Research"
author: "Alexander Roth"
date: 2022-03-13
slug: "research"
---
### Topics

- Decarbonisation of the energy system
- Integration of renewable energy into the electricity system
- Electricity storage
- Externalities of energy infrastructure

### Methods

- Energy market modelling
- (Linear) numerical methods
- Applied econometrics
- Causal inference

---
# Publications

## 1. Academic

### Peer-reviewed
- [**DIETERpy: A Python framework for the Dispatch and Investment Evaluation Tool with Endogenous Renewables**](https://www.sciencedirect.com/science/article/pii/S2352711021000947),
2021, *SoftwareX 15* (with C. Gaete-Morales, M. Kittel, W.-P. Schill)

### Work in progress
- **Why geographical balancing decreases electricity storage needs: a model-based illustration for Europe** (with W.-P. Schill)
- **External costs of electricity transmission infrastructure: evidence from well-being data**  
(with L. Sarmiento, A. Zaklan, A. Zerrahn)
- **The effect of wind power plants on health** (with C. Krekel and J. Rode)

## 2. Policy

- [**Zukunft des europäischen Energiesystems: Die Zeichen stehen auf Strom**](/documents/2022_strom.pdf), 2022, *DIW Wochenbericht 89 (6)* (with F. Holz, R. Sogalla, F. Meißner, G. Zachmann, B. McWilliams, C. Kemfert)
- [**Decarbonisation of the energy system**](/documents/2022_decarbonisation_policy.pdf), 2022, *Policy Contribution 01/2022*, Bruegel (with G. Zachmann, F. Holz, C. Kemfert, B. McWilliams, F. Meissner, R. Sogalla)
- [**Decarbonisation of energy**](/documents/2021_decarbonisation.pdf), 2021, Publication for the committee on Industry, Research and Energy (ITRE), *Policy Department for Economic, Scientiﬁc and Quality of Life Policies, European Parliament*, Luxembourg (with G. Zachmann, F. Holz, B. McWilliams, F. Meissner, R. Sogalla, C. Kemfert)
- [**Low-Carbon Transport Policies for Ukraine**](/documents/2021_ukraine.pdf), 2021, *Low Carbon Ukraine Policy Note*
- [**Learning for Decarbonisation: Start Early, Concentrate on Promising Technologies, Exploit Regional Strength and Work with Your National System**](/documents/2018_learning.pdf), 2018, *COP21 RIPPLES* (with the COP21 RIPPLES Consortium)
- [**Is the European Automotive Industry Ready for the Global Electric Vehicle Revolution?**](/documents/2018_automotive.pdf), 2018, *Policy Contribution 26/2018*, Bruegel (with G. Fredriksson, S. Tagliapietra & R. Veugelers)
- [**Determining Future Comparative and Technological Advantage in Low-Carbon Technologie**](/documents/2018_RIPPLES.pdf) in: COP21: Results and Implications for Pathways and Policies for Low Emissions European Societies, 2018, *COP21 RIPPLES* (with G. Zachmann)
- [**The Impact of Brexit on the EU Energy System**](/documents/2017_impact_brexit.pdf), Study for the ITRE Committee, 2017, *European Parliament - Policy Department A: Economic and Scientiﬁc Policy* (with G. Fredriksson, S. Tagliapietra & G. Zachmann)
- [**The Single Monetary Policy and Its Decentralised Implementation: An Assessment, In-Depth Analysis for the Monetary Dialogue**](/documents/2017_single_monetary.pdf), 2017, *European Parliament - Policy Department A: Economic and Scientiﬁc Policy* (with F. Papadia)

### Outreach
- [**Still on the Road? Assessing Trump's Threat to European Cars**](https://www.bruegel.org/2018/03/still-on-the-road-assessing-trumps-threat-to-european-cars/), 2018, *Bruegel Blog Post* (with G. Fredriksson & S. Tagliapietra)
- [**Mini-BOT in the Government Programme of the Five Star Movement and the League**](https://www.bruegel.org/2018/06/mini-bots-in-the-government-programme-of-the-five-star-movement-and-the-league/), 2018, *Bruegel Blog Post* (with F. Papadia)
- [**Understanding (the Lack of) German Public Investment**](https://www.bruegel.org/2018/06/understanding-the-lack-of-german-public-investment/), 2018, *Bruegel Blog Post* (with G. Wolff)
- [**No Financial Meltdown**](https://www.bruegel.org/2018/01/no-financial-meltdown/), 2018, *Bruegel Blog Post* (with G. Zachmann)
- [**India's Trade Ties with the UK and EU**](https://www.bruegel.org/2017/10/indias-trade-ties-with-the-uk-and-eu/), 2017, *Bruegel Blog Post* (with M. Demertzis)
- [**The Eurosystem – Too Opaque and Costly?**](https://www.bruegel.org/2017/11/the-eurosystem-too-opaque-and-costly/), 2017, *Bruegel Blog Post* (with F. Papadia)
- [**The Size and Location of Europe's Defence Industry**](https://www.bruegel.org/2017/06/the-size-and-location-of-europes-defence-industry/), 2017, *Bruegel Blog Post*
- [**What Has Driven the Votes for Germany's Right-Wing Alternative Für Deutschland?**](https://www.bruegel.org/2017/10/what-has-driven-the-votes-for-germanys-right-wing-alternative-fur-deutschland/), 2017, *Bruegel Blog Post* (with G.
Wolff)
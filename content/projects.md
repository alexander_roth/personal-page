---
title: "Projects"
author: "Alexander Roth"
date: 2022-03-13
slug: "projects"
---

<br/>

<a href="https://openenergytracker.org"><img src="/images/koavtracker.png" title = "KoaVTracker" alt="openenergytracker website"
style="width: 150px; float: left; margin-right:25px; margin-top:25px; margin-bottom:5px"/>

# [Open Energy Tracker](https://openenergytracker.org)

The *Open Energy Tracker* visualizes the goals and progress of governmental goals in different areas of the energy transition.

<br/>

<a href="https://www.decarb.world"><img src="/images/dcarb.png" title = "d\carb future economy forum" alt="d\carb logo"
style="width: 150px; float: left; margin-right:25px; margin-top:25px; margin-bottom:5px"/>
  
# [d\carb future economy forum](https://www.decarb.world)

The *d\carb future economy forum* was founded by students interested in climate policy. We stand for scientifically informed critical debates, plural and accessible to the general public.

<br/>

<a href="https://open.spotify.com/show/4cMpPw9DSyIWPWBWYIDYXh"><img src="/images/untangled.png" title = "EE Untangled Podcast" alt="eu untangled logo" 
style="width: 150px; float: left; margin-right:25px; margin-top:25px; margin-bottom:5px"/>

# [EU Untangled](https://open.spotify.com/show/4cMpPw9DSyIWPWBWYIDYXh)

EU politics and policies are messy and filled with jargon hard to understand, which makes it almost impossible to keep up with the latest developments in the EU. But trust us – there is plenty of exciting stuff all over the continent that you may not even be aware of! EU Untangled is here to help you make sense of this mess and speak of the politics and policies in a simple - yet informative - way so you can understand what's going on behind the headlines.